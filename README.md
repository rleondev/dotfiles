### some configuration files

 - [.cycle_workspaces.sh](./.cycle_workspaces.sh) - xfce script to modify cycle applications selector
 - [.tmux.conf](./.tmux.conf) - simple tmux configuration
 - [.vimrc](./.vimrc) - base vim configuration
 - [init.el](./init.el) - first base emacs configuration
 - [doom](./doom/) - doom emacs distribution personal configuration
