"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set history=500
filetype plugin on
filetype indent on
set autoread
set nu
set mouse=a
let mapleader = ","

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set so=7
let $LANG='en'
set langmenu=en
set wildmenu
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif
set ruler
set cmdheight=2
set hid
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set ignorecase
set smartcase
set hlsearch
set incsearch
set lazyredraw
set magic
set showmatch
set mat=2
set noerrorbells
set novisualbell
set t_vb=
set tm=500
set foldcolumn=1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif
colorscheme molokai
set background=dark
set encoding=utf8
set ffs=unix,dos,mac
highlight Search ctermbg=yellow ctermfg=black guibg=yellow guifg=black
set cursorline
hi CursorLine cterm=NONE ctermbg=236
set colorcolumn=80,120
hi ColorColumn ctermbg=236

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nowb
set noswapfile
set isfname-=:

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set lbr
set tw=500
set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <space> /
map <c-space> ?
map <silent> <leader><cr> :noh<cr>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
map <leader>b :ls<cr>:b<Space>
map <leader>bd :Bclose<cr>
map <leader>ba :bufdo bd<cr>
map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>
let g:lasttab = 1
map <leader>cd :cd %:p:h<cr>:pwd<cr>
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
set splitright
set splitbelow

""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
set laststatus=2
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map 0 ^
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun
if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <leader>ss :setlocal spell!<cr>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm
map <leader>v :e ~/.vimrc<cr>
map <leader>k :e ~/.notes<cr>
map <leader>pp :setlocal paste!<cr>
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"
augroup myCmds
au!
autocmd VimEnter * silent !echo -ne "\e[2 q"
augroup END
au BufReadPre FILETYPE au! PluginNotes
au BufReadPre FILETYPE au! PluginXoloxMisc
map <leader>t :terminal ++curwin<cr>
map <leader>vt :vertical terminal<cr>
tnoremap <Esc> <C-\><C-n>
map <leader>m :marks<cr>
map <leader>r :registers<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
    let l:currentBufNum = bufnr("%")
    let l:alternateBufNum = bufnr("#")
    if buflisted(l:alternateBufNum)
        buffer #
    else
        bnext
    endif
    if bufnr("%") == l:currentBufNum
        new
    endif
    if buflisted(l:currentBufNum)
        execute("bdelete! ".l:currentBufNum)
    endif
endfunction
function! CmdLine(str)
    call feedkeys(":" . a:str)
endfunction
function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"
    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")
    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif
    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'valloric/youcompleteme'
Plugin 'scrooloose/syntastic'

Plugin 'tpope/vim-commentary'
Plugin 'airblade/vim-gitgutter'
Plugin 'easymotion/vim-easymotion' " <leader><leader>s

Plugin 'scrooloose/nerdtree'
map <leader>nt :NERDTreeToggle<CR>

Plugin 'majutsushi/tagbar'
map <leader>tb :TagbarToggle<CR>

Plugin 'junegunn/fzf'
map <leader>f :FZF<CR>

" Plugin 'powerline/powerline'

Plugin 'sjl/gundo.vim'
map <leader>u :GundoToggle<CR>

Plugin 'mileszs/ack.vim'
nnoremap <Leader>a :Ack!<Space>

Plugin 'itchyny/lightline.vim'
Plugin 'itchyny/vim-gitbranch'
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'filename': 'LightlineFilename',
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }

function! LightlineFilename()
  return &filetype ==# 'vimfiler' ? vimfiler#get_status_string() :
        \ &filetype ==# 'unite' ? unite#get_status_string() :
        \ &filetype ==# 'vimshell' ? vimshell#get_status_string() :
        \ expand(@%) !=# '' ? expand(@%) : '[No Name]'
endfunction

" Plugin 'jceb/vim-orgmode'

let g:unite_force_overwrite_statusline = 0
let g:vimfiler_force_overwrite_statusline = 0
let g:vimshell_force_overwrite_statusline = 0

call vundle#end()            " required
filetype plugin indent on    " required

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => cheat sheets, reminders, ...
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" :[vertical]term [++open] [++curwin] => open terminal [dont close when exit] [in same window]
" :r !<command> => execute command and paste result
" :f! <new_name> => rename current buffer
" {c,d,v}a{w,p,t} all word or pharagraph or tag
" ; or, => repeat or reverse search with t or f
" cc delete and edit line
" dp in vimdiff => move changes
" ctrl+o in insert mode, temporal normal mode for execute one command
" ctrl+r{register} in insert or comment mode, paste from registers
" ctrl+r = {command} in insert mode, paste command result
" . in command mode, actual line
" % in command mode, all lines
" :/TEXT/[OFFSET],/TEXT/[OFFSET]{command} ejecuta command on these lines like !sort
" :{range}t|m{address} copy|move range to address, 'd', 'y' also works without address
" :{range}normal {command normal mode} => exec command on this lines like A;
" = indent
" ctrl+r Ctrl+v insert current word in command mode
" q: show window with command history
" q/ show window with search history
" :write !sh execute buffer content in shell
" :source ??
" :args ??
